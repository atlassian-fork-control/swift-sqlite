//
//  ResultRow.swift
//  sqlite
//
//  Created by Nikolay Petrov on 10/27/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

public struct SQLiteResultRow {
    public let statement: SQLiteStatement

    public init?(statement: SQLiteStatement) throws {
        self.statement = statement

        let runCode = sqlite3_step(statement.sqlite3_stmt)
        switch runCode {
        case SQLITE_ROW:
            break
        case SQLITE_DONE:
            return nil
        default:
            throw SQLiteError(code: runCode)
        }
    }

    public func next() throws -> SQLiteResultRow? {
        return try SQLiteResultRow(statement: statement)
    }

    public func name(from: Int) throws -> String {
        guard let str = sqlite3_column_name(statement.sqlite3_stmt, Int32(from)) else {
            throw SQLiteError(code: SQLITE_NOTFOUND)
        }
        return String(cString: str)
    }

    public func type(from: Int) throws -> SQLiteColumnType {
        let rawType = sqlite3_column_type(statement.sqlite3_stmt, Int32(from))
        return SQLiteColumnType(rawValue: rawType)!
    }

    public func blob(from: Int) throws -> Data {
        let count = sqlite3_column_bytes(statement.sqlite3_stmt, Int32(from))
        guard let bytes = sqlite3_column_blob(statement.sqlite3_stmt, Int32(from)) else {
            throw SQLiteError(code: SQLITE_NOTFOUND)
        }
        return Data(bytes: bytes, count: Int(count))
    }

    public func double(from: Int) throws -> Double {
        return sqlite3_column_double(statement.sqlite3_stmt, Int32(from))
    }

    public func int(from: Int) throws -> Int32 {
        return sqlite3_column_int(statement.sqlite3_stmt, Int32(from))
    }

    public func int64(from: Int) throws -> Int64 {
        return sqlite3_column_int64(statement.sqlite3_stmt, Int32(from))
    }

    public func string(from: Int) throws -> String {
        guard let str = sqlite3_column_text(statement.sqlite3_stmt, Int32(from)) else {
            throw SQLiteError(code: SQLITE_NOTFOUND)
        }
        return String(cString: str)
    }

    public func count() throws -> Int {
        return Int(sqlite3_column_count(statement.sqlite3_stmt))
    }

    public func close() throws {
        try statement.close()
    }
}
